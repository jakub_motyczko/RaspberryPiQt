# Build Qt 5.14 for RaspberryPi 4


## create dirs
```
mkdir /opt/raspi/

chown kuba.users /opt/raspi/

cd /opt/raspi/

mkdir sysroot sysroot/usr sysroot/opt
```

## sync raspi sysroot
```
rsync -avz raspi4:/lib sysroot

rsync -avz raspi4:/usr/include sysroot/usr

rsync -avz raspi4:/usr/lib sysroot/usr

rsync -avz raspi4:/opt/vc sysroot/opt
```

## fix symlinks
```
wget https://raw.githubusercontent.com/riscv/riscv-poky/master/scripts/sysroot-relativelinks.py

chmod +x sysroot-relativelinks.py

./sysroot-relativelinks.py sysroot
```

## download compiler

```
https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz

tar xf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
```

## download Qt

```
wget http://download.qt.io/official_releases/qt/5.14/5.14.0/single/qt-everywhere-src-5.14.0.tar.xz

tar xf qt-everywhere-src-5.14.0.tar.xz

cd qt-everywhere-src-5.14.0
```

## fix qmake.conf

```
sed -i -e 's/-lEGL/-lbrcmEGL/g' qtbase/mkspecs/devices/linux-rasp-pi4-v3d-g++/qmake.conf

sed -i -e 's/-lGLESv2/-lbrcmGLESv2/g' qtbase/mkspecs/devices/linux-rasp-pi4-v3d-g++/qmake.conf
```

## config Qt

```
./configure -release -opengl es2 -device linux-rasp-pi-g++ -device-option CROSS_COMPILE=/opt/raspi/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf- -sysroot /opt/raspi/sysroot -opensource -confirm-license -skip qtwayland -skip qtlocation -skip qtscript -make libs -prefix /opt/raspi/qt5.14 -extprefix /opt/raspi/qt5.14 -hostprefix /opt/raspi/qt5.14 -no-use-gold-linker -v -no-gbm
```

## build

```
make -j`nproc`
```




